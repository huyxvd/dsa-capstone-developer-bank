﻿public class CustomerQueue
{
    public Customer[] customers; // lưu trữ mảng khách hàng rút tiền
    /// <summary>
    /// Khởi tạo queue
    /// </summary>
    /// <param name="maxQueue"> số phần tử tối đa queue có thể chứa </param>
    public CustomerQueue(int maxQueue)
    {
        customers = new Customer[maxQueue];
    }

    public int Count => throw new NotImplementedException(); // số lượng khách hàng trong queue

    public void Enqueue(Customer customer)
    {
        // TODO: Thêm customer vào cuối hàng đợi.
    }

    public Customer Dequeue()
    {
        // TODO: Lấy và xóa khách hàng ở đầu hàng đợi.
        throw new NotImplementedException();
    }

    public Customer Peek()
    {
        // TODO: Lấy khách hàng ở đầu hàng đợi mà không xóa nó.
        throw new NotImplementedException();
    }

    public void Clear()
    {
        // TODO: Xóa tất cả các khách hàng trong hàng đợi.
    }

    public Customer[] Get3NextCustomer()
    {
        // TODO: lấy ra 3 phần tử gần nhất trong queue
        throw new NotImplementedException();
    }

    public decimal SumOfMoneyInQueue()
    {
        // TODO: Tổng tiền trong queue
        throw new NotImplementedException();
    }
}
